﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile1
{
    class Program
    {

        static void AddPersonInfo()
        {
            try
            {
                Console.WriteLine("Adding A person");
                Console.WriteLine("Enter  name: ");
                string name = Console.ReadLine();

                //  Console.Write("Enter age: ");
                // int age = int.Parse(Console.ReadLine()); // ex FormatException
                Console.WriteLine("Enter  age: ");
                string ageStr = Console.ReadLine();
                int age = int.Parse(ageStr);// ex FormatException
                /* if (!int.TryParse(ageStr, out age))
                 {
                     Console.WriteLine("Error: Age must be a valid integer.");
                     return;
                 }*/
                Console.Write("Enter city: ");
                string city = Console.ReadLine();
                Person person = new Person(name, age, city); // ex ArgumentException
                People.Add(person);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error: invalid numerical input");
            }

        }
        static void ListAllPersonsInfo()
        {

            foreach (Person list in People)
            {
                Console.WriteLine(list);
            }
        }
        static void FindPersonByName()
        {
            Console.Write("Enter partial person name: ");
            string searchStr = Console.ReadLine();
            Console.WriteLine("Matches found:");
            var matchesList = People.Where(p => p.Name.Contains(searchStr)).ToList(); // LINQ
            if (matchesList.Count() > 0)
            {
                foreach (Person p in matchesList)
                {
                    Console.WriteLine(p);
                }
            }
            else
            {
                Console.WriteLine("No matches found");
            }

            /*
            foreach (Person p in People)
            {
                if (p.Name.Contains(searchStr))
                {
                    Console.WriteLine(p);
                }
            }*/
        }
        
        static void FindPersonYoungerThan()
        {
            Console.Write("Enter maximum age: ");
            if (!int.TryParse(Console.ReadLine(), out int maxAge))
            {
                Console.WriteLine("Invalid input");
                return;
            }
            // using a foreach loop with a condition
            Console.WriteLine("Peple at that age or younger:");
            foreach (Person p in People)
            {
                if (p.Age <= maxAge)
                {
                    Console.WriteLine(p);
                }
            }
            // using LINQ
            Console.WriteLine("Peple at that age or younger (using LINQ):");
            var youngerList = People.Where(p => p.Age <= maxAge);
            foreach (Person p in youngerList)
            {
                Console.WriteLine(p);
            }

        }

        static void ReadAllPeopleFromFile()
        {
            try
            {
                if (!File.Exists(FILEDATA))
                {
                   return;// it's okay if the file  does not exist yet
             }

                string[] linesArray = File.ReadAllLines(FILEDATA); //ex IOException , SystemException


                foreach (string line in linesArray)
                {
                    try
                    {
                        string[] data = line.Split(';');
                        if (data.Length != 3)
                        {
                            throw new FormatException(" Invalid number of items");
                            //OR : -> Console.WriteLine("Error ..."); continue;
                        }

                        string name = data[0];
                        int age = int.Parse(data[1]); // ex FormatException
                        string city = data[2];
                        Person person = new Person(name, age, city);// ArgumentExeption
                        People.Add(person);
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine($" Error(skipping line): { ex.Message} in: \n {line}");
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }


        static void SaveAllPeopleToFile()
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (Person person in People)
                {
                    linesList.Add($"{person.Name};{person.Age};{person.City}");
                }
                File.WriteAllLines(FILEDATA, linesList); // ex IOException, SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
        // 3 ways to get out of the loop: break, return, throw exception
        static List<Person> People = new List<Person>();

        const string FILEDATA = @"..\..\people.txt";

        static int getMenuChoice()
        {
            while (true)
            {
                Console.WriteLine("What do you want to do?\n"
                        + "1. Add person info\n"
                        + "2. List persons info\n"
                        + "3. Find a person by name\n"
                        + "4. Find all persons younger than age\n"
                        + "0. Exit\n"
                        + "Your choice is: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    // 3 ways to get out of the loop: break, return, throw exception
                    return choice;
                }
                Console.WriteLine("Invalid input, must be numerical. Try again.");
            }
        }
        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveAllPeopleToFile();
                        Console.WriteLine("Good bye!.");
                        return;// exit the program
                    default:
                        Console.WriteLine("Invalid choice, try again");
                        break;
                }
            }
            /*
            try
            {
                // Person p1 = new Person();
                //p1.Name = "Jerry";// call to setter
                //string n1 = p1.Name;//call to getter

                // object initializer, may not initialize all fields/properties
                Person p2 = new Person() { Name = "Eva", Age = 76, City = "Montreal" };
                string p2s = $"Person Name ={p2.Name} , Age is {p2.Age} ,City is ={p2.City}";
                Console.WriteLine(p2s);

            }
            finally
            {
                Console.Write("press any key to finish");
                Console.ReadKey();
            }*/

        }
    }
}
