﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermPerson
{
    /// <summary>
    /// Interaction logic for Passport.xaml
    /// </summary>
    public partial class Passport : Window
    {
        DataLayer.Passport currPerson;
        public Passport(DataLayer.Passport pass)
        {
            try
            {
                InitializeComponent();
                if(pass != null)
                {
                    currPerson = pass;
                    tbPass.Text = currPerson.PassportNumber;
                    currOwnerImage = pass.Photo;
                    imageViewer.Source = Utils.ByteArrayToBitmapImage(pass.Photo); // ex
                    btSave.Content = "Update";
                }
                else {
                    btSave.Content = "Save";
                }
                Globals.ctx = new DataLayer.PersonPassportDbContext(); // ex SystemException
               
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }

        }
        
        bool AreInputsValid()
        {
            if (!Regex.IsMatch(tbPass.Text, @"^[A-Z]{2}[0-9]{6}$"))
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        byte[] currOwnerImage;
        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            // dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            try
            {
                DataLayer.Passport p = new DataLayer.Passport { PassportNumber = tbPass.Text , Photo = currOwnerImage, };
                Globals.ctx.Passports.Add(p);
                Globals.ctx.SaveChanges();
                tbPass.Text = "";
                
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
