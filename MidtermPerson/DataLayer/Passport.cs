﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermPerson.DataLayer
{
   public class Passport
    {
        public int Id { get; set; }

        [Required]
        public byte[] Photo { get; set; }
        [Required]
        [StringLength(8)]
        public string PassportNumber { get; set; }
        public virtual Person Person { get; set; }

        
    }
}
