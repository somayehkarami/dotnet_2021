﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MidtermPerson
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new DataLayer.PersonPassportDbContext(); // ex SystemException
                DataLayer.Person person = (DataLayer.Person)lvPerson.SelectedItem;
                //ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
    

        private void lvPerson_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataLayer.Person person = (DataLayer.Person)lvPerson.SelectedItem;
            if (person == null) return;
            AddEditDialog PersonDialog = new AddEditDialog(person) ;
            PersonDialog.Owner = this;
            PersonDialog.ShowDialog();
            //ReloadRecords();
        }

        private void miAddPerson_Click(object sender, RoutedEventArgs e)
        {

            DataLayer.Person person = (DataLayer.Person)lvPerson.SelectedItem;
            if (person == null) return;
            AddEditDialog PersonDialog = new AddEditDialog(person) ;
            PersonDialog.Owner = this;
            PersonDialog.ShowDialog();
            //ReloadRecords();

         
            
        }
    }
}
