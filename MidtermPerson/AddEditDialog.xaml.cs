﻿using MidtermPerson.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermPerson
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        public AddEditDialog(DataLayer.Person person)
        {
            InitializeComponent();
           //lvPerson.ItemsSource = (from p in Globals.ctx.People select p).ToList<Person>();
        }

       

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if (!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age invalid");
                return;
            }
            try
            {
                Person person = new Person { Name = name, Age = age };
                Globals.ctx.People.Add(person);
                Globals.ctx.SaveChanges();
                tbName.Text = "";
                tbAge.Text = "";
                // refresh the list for database
                //lvPerson.ItemsSource = (from p in Globals.ctx.People select p).ToList();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed:\n" + ex.Message);
            }
        }
    }
}
