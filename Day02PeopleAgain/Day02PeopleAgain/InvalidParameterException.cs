﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class InvalidParameterException : Exception
    {
        public InvalidParameterException(String msg) : base(msg)
        {

        }

    }
}
