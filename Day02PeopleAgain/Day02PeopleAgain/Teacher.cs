﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    public class Teacher : Person
    {



        //string Subject; // 1-50 characters, no semicolons
        //int YearsOfExperience; // 0-100

        public Teacher(string dataLine): base()
        {
            String[] data = dataLine.Split(';');

            if (data.Length != 5)
            {
                throw new InvalidParameterException("Invalid number of items in line");
            }
            if (data[0] != "Teacher")
            {
                throw new InvalidParameterException("Data line does not describe Teacher");
            }

            Name = data[1];
            int age;
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
            Subject = data[3];
            int yearsOfExperience;
            if (!int.TryParse(data[4], out yearsOfExperience))
            {
                throw new InvalidParameterException("Years of experience in data line must be an integer");
            }
            YearsOfExperience = yearsOfExperience;
        }



        public Teacher(string name, int age, string subject, int yearsOfExperience) : base(name, age)
        {
            
            Subject = subject;
            YearsOfExperience = yearsOfExperience;
        }
        private string _subject;
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length < 0 || value.Length > 150)
                    throw new ArgumentException("age must be between 0 and 150 .");
                _subject = value;
            }
        }
        private int _yearsOfExperience;
        public int YearsOfExperience
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if (value < 0 || value > 150)
                    throw new ArgumentException("age must be between 0 and 150 .");
                _yearsOfExperience = value;
            }
        }
        public override string ToString()

        {
            return $"Teacher:{Name} is  {Age}y/o, teaching  {Subject}  with{YearsOfExperience} years of Experience ";
        }
    }

}



