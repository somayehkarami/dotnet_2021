﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {
        const string DataFileName = @"..\..\people.txt";
        static void ReadDataFromFile()
        {
            try
            {
                string[] linesArray = File.ReadAllLines(DataFileName); //ex IOException , SystemException
                foreach (string line in linesArray)
                {
                    try
                    {
                        string type = line.Split(';')[0];
                        switch (type)
                        {
                            case "Person":
                                Person p = new Person(line);
                                PeopleList.Add(p);
                                break;
                            case "Teacher":
                                Teacher t = new Teacher(line);
                                PeopleList.Add(t);
                                break;
                            case "Student":
                                Student s = new Student(line);
                                PeopleList.Add(s);
                                break;
                            default:
                                break;

                        }
                    }
                    catch (InvalidParameterException ex)
                    {
                        Console.WriteLine($"Error parsing line: {ex.Message}\n >> {line}"); // for stress- testing parsing
                    }

                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        const string WriteDataFileName = @"..\..\byname.txt";
        static void ToDataString()
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Person person in PeopleList)
                {

                    var result = PeopleList.OrderBy(x => x.Name).ToList();
                    foreach (var p in result)
                    {
                        //if (p.GetType() == typeof(Person))
                        //if (p is Person)
                            //Console.WriteLine(p.ToString());
                            //  Console.WriteLine($"{p.Name}");
                            Console.WriteLine(p.ToString());
                    }

                    linesList.Add($"{person.Name};{person.Age};");
                    

                }
                File.WriteAllLines(WriteDataFileName, linesList); // ex IOException, SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }




        public static List<Person> PeopleList = new List<Person>();

        static void Main(string[] args)
        {
            try
            {
                // Person p1 = new Person(); // won't work thanks to protected access to this constructor

                ReadDataFromFile();
                ToDataString();
                foreach (Person p in PeopleList)
                {
                    Console.WriteLine(p.ToString());
                }
                Console.WriteLine("---------- List of only Students-------------");
                foreach (Person p in PeopleList)
                {
                    if (p is Student)
                        Console.WriteLine(p.ToString());
                }
                Console.WriteLine("---------- List of only Teachers-------------");
                foreach (Person p in PeopleList)
                {
                    if (p is Teacher)
                        Console.WriteLine(p.ToString());
                }
                Console.WriteLine("---------- List of only Persons-------------");
                foreach (Person p in PeopleList)
                {
                    //                  if( !(p is Teacher)&& !(p is Student))
                    //                      Console.WriteLine(p.ToString());


                    if (p.GetType() == typeof(Person))
                    {    // output: True
                        Console.WriteLine(p.ToString());
                    }
                }
                int count = 0;
                double avg = 0;
                double sum = 0;
                foreach (Person item in PeopleList)
                {
                    if (item.GetType() == typeof(Student))
                    {
                        Student s = (Student)item;
                        Console.WriteLine(s.GPA);
                        count++;
                        sum += (double)s.GPA;

                    }

                }
                avg = sum / count;
                Console.WriteLine(sum);
                Console.WriteLine(avg);
            
             var result = PeopleList.OrderBy(x => x.Name).ToList();
                foreach (var p in result)
                {
                   //if (p.GetType() == typeof(Person))
                        if (p is Teacher)
                            //Console.WriteLine(p.ToString());
                            //  Console.WriteLine($"{p.Name}");
                            Console.WriteLine(p.ToString());
                }
                
              

            }
            finally
            {
                Console.Write("Press any key.");
                Console.ReadKey();
            }
        }

    
    }
}
