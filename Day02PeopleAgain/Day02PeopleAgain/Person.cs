﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    public class Person
    {
        protected Person() { }
        public Person(string name, int age)// InvalidParamreterEx
        {
            Name = name;
            Age = age;
        }

        public Person(string dataLine)
        {
            String[] data = dataLine.Split(';');

            if (data.Length != 3)
            {
                throw new InvalidParameterException("Person data line must have 3 fields");
            }
            if (data[0] != "Person") // double-check if this line describes a person
            {
                throw new InvalidParameterException("Data line does not describe Person");
            }

           //string name = data[1];
            Name = data[1];
            if (!int.TryParse(data[2], out int age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
        }
        private string _name;
        public string Name //Property
        {
            get
            {
                return _name;
            }
            set
            {
                //if (value.Length < 0 || value.Length > 150 || value.Contains(";"))
                if (!Regex.IsMatch(value, @"^[^;]{0,150}$")) //, RegexOptions.IgnoreCase))
                {
                    throw new ArgumentException("Name must be 2-100 characters long, no semicolons");//ex
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                    throw new ArgumentException("age must be between 0 and 150 .");
                _age = value;
            }
        }
        public override string ToString()
        {
            return $"Person {Name} is  {Age} y/o";
        }

    }
}

