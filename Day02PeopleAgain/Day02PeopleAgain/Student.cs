﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
   public class Student : Person
    {
        public Student(string name, int age, string program, double gPA) : base(name, age)
        {
            Program = program;
            GPA = gPA;
        }

        public Student(string dataLine) 
        {
            String[] data = dataLine.Split(';');

            if (data.Length != 5)
            {
                throw new InvalidParameterException("Student data line must have 5 fields");
            }
            if(data[0] != "Student")
            {
                throw new InvalidParameterException("Data line does not describe Student");
            }
                Name = data[1];
            int age; // out parameter cannot be a property
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
            // parse the rest of fields
            Program = data[3];
            double gpa;
            if (!double.TryParse(data[4], out gpa))
            {
                throw new InvalidParameterException("GPA in data line must be numerical");
            }
            GPA = gpa;
        }



        private string _program;
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 0 || value.Length > 150 || value.Contains(";"))
                    throw new ArgumentException("Program must be between 0 and 150 and no semicolons");
                _program = value;
            }
        }
        private double _gPA;
        public double GPA
        {
            get
            {
                return _gPA;
            }
            set
            {
                if (value < 0 || value > 4.3)
                    throw new ArgumentException("GPA must be between 0 and 4.3.");
                _gPA = value;
            }
        }
        public override string ToString()

        {
            return $"Student {Name} is  {Age}y/o,  studying {Program} ,has {GPA} GPA";
        }
    }
}
