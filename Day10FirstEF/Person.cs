﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
   public class Person
    {
        public int Id { get; set; }
        [Required]// means  non-null
        [StringLength(50)] //nvarchar(50)
        public string Name { get; set; }
        [Required]
        [Index] // not unique, speeds up lookup opration
        public int Age { get; set; }
      //  [NotMapped] // in memory
     //   public string Comment { get; set; }


    }
}
