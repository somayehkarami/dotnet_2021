﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person
    {
        public Person() { }

        public Person(string name, int age, string city)
        {
            Name = name;

            Age = age;

            City = city;

        }

        private string _name;
        public string Name
        { // property
            get
            {
                return _name;
            }
            set
            {
                //if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                if (!Regex.IsMatch(value, @"^[^;]{2,100}$")) //, RegexOptions.IgnoreCase))
                {
                    throw new ArgumentException("Name must be 2-100 characters long, no semicolons");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age // Age 0-150 (Property)
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                    throw new ArgumentException("age must be between 0 and 150 .");
                _age = value;
            }
        }


        private string _city;

        

        public string City //throws InvalidDataException
        { // City 2-100 characters long, not containing semicolons
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                    throw new ArgumentException("City has a maximum length of 100.");
                _city = value;
            }
        }

    }

}
