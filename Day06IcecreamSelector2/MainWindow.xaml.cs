﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace Day06IcecreamSelector2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       // ArrayList myList;
        //string currentitem = string.Empty;
        //int index = 0;
        public MainWindow()
        {
            InitializeComponent();
          //  myList = loadListbox();
          //  leftListbox.ItemsSource = myList;
        }
        // my solution=========
        private ArrayList loadListbox()
        {
            ArrayList list = new ArrayList();
            list.Add("Vanilla");
            list.Add("Chocolate");
            list.Add("Strawberry");
            list.Add("Peach");
            return list;
        }
        //==============
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            //my soulotion
            /*currentitem = leftListbox.SelectedValue.ToString();
            index = leftListbox.SelectedIndex;
            rightListbox.Items.Add(currentitem);*/

            //MessageBox.Show(currentitem);

            //=============================
            // var selItems = lvFlavours.SelectedItem;(for one Item)
            var selItems = lvFlavours.SelectedItems;
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "You must select at least one flavour to add", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            foreach (ListBoxItem item in selItems)
            {
                lvSelected.Items.Add(item.Content); // add a string
            }
            lvFlavours.UnselectAll();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {

            //rightListbox.Items.Remove(rightListbox.SelectedItem);
            var selItems = lvSelected.SelectedItems; // collection of string
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "You must select at least one flavour to delete", "Input error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            // foreach loop can't modify the collection it is iterating over
            // we must first copy items into a list or array
            string[] selArray = selItems.Cast<string>().ToArray();// cast Ilist toArray
            foreach (string item in selArray)// go over the List
            {
                lvSelected.Items.Remove(item);
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            // ask confirmation before deleting all - use MessageBox
            if (MessageBox.Show(this, "Are you sure you want to delete all scoops in this order?",
                    "Confirmation", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                lvSelected.Items.Clear();
            }
        }
            private void leftListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
