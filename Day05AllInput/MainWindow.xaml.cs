﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05AllInput
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //string result = lblSlider.Text;
            //result = sliderTemp.Value.ToString();
            /* try
             {
                 string temp = sliderTemp.Value.ToString();
                 Label lblSlider = new Label();
                 lblSlider.Content = temp;
             }
             catch (NullReferenceException )
             {



             }    */

        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if (string.IsNullOrEmpty(name)) //if(name =="")
            {
                MessageBox.Show(this, "Please enter a name", "Input error", MessageBoxButton.OK, MessageBoxImage.Exclamation);


                return;
            }
            string age;
            if (rbAgeBelow18.IsChecked == true)
            {
                age = "below18";
            }
            else if (rbAge18to35.IsChecked == true)
            {
                age = "18to35";
            }
            else if (rbAge36andUp.IsChecked == true)
            {
                age = "16andup";
            }
            else
            { // Should never happen
                MessageBox.Show( this,"Age must  be selected" ,"Internal Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            // pets
            List<string> petsList = new List<string>();
            if (cbPetCat.IsChecked == true)
            {
                petsList.Add("cat");
            }
            if (cbPetDog.IsChecked == true)
            {
                petsList.Add("dog");
            }
            if (cbPetOther.IsChecked == true)
            {
                petsList.Add("other");
            }
            string pets = string.Join<string>(",", petsList);
            //// alternative: string continent = comboContinent.Text; Jessica Soulution
            string continent = comboContinent.SelectedValue?.ToString();// conditional call (give null)
            if (continent == null)
            {
                MessageBox.Show(this, "Please select a continent", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double tempC = sliderTemp.Value;
           // int temp = (int)sliderTemp.Value;
           // string tempC = temp.ToString();

           
            string line =$"{name};{age};{pets};{continent};{ tempC}";
            File.AppendAllText(@"..\..\output.txt", line + "\n");
            MessageBox.Show(this, "Data appended to file", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);

            Console.WriteLine(line);
            //{0:0,0.0}add to Xaml


        }
    }
}
