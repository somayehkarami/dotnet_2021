﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day00Cone1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("How big does your cone needs to be?");
			int n = Convert.ToInt32(Console.ReadLine());
			for (int i = 1; i <= n; i++)
			{
				for (int j = 1; j < i; j++)
				{
					Console.Write(" ");
				}
				for (int k = 1; k <= (n * 2-(i*2-1)); k++)
				{
					Console.Write("*");
				}
			
				Console.WriteLine();
			}
			Console.ReadLine();

		}
	}
}
