﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day00People
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] namesArray = new string[4];
                //int[] agesArray = new int[4];
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine("Enter name #{0}:", i + 1);
                    string name = Console.ReadLine();
                    namesArray[i] = name;

                    Console.WriteLine("Enter age #{0}:", i + 1);
                    //string name = Console.ReadLine();

                }
                // print them back
                Console.Write("you Entered: ");
                for (int i = 0; i < 4; i++)
                {
                    Console.Write("{0}{1}: ", i == 0 ? "" : ",", namesArray[i]);
                }

            }
            finally
            {
                Console.Write("press any key to finish");
                Console.ReadKey();
            }
        }
    }
}
