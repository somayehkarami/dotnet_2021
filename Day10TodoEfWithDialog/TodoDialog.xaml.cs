﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day10TodoEfWithDialog
{
    /// <summary>
    /// Interaction logic for TodoDialog.xaml
    /// </summary>
    public partial class TodoDialog : Window
    {
        public Todo currTodo; // null if adding, non-null if editing
                              //TodoDBContext ctx;
        public TodoDialog(Todo td = null)
        {


            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum)).Cast<Todo.StatusEnum>();
            comboStatus.SelectedIndex = 0;
            dpDueDate.SelectedDate = DateTime.Today;
            if (td != null)
            {

                currTodo = td; // save todo being edited
                lblId.Content = td.Id;
                tbTask.Text = td.Task;
                dpDueDate.SelectedDate = td.DueDate;
                comboStatus.SelectedValue = td.Status;
                btsave.Content = "Update Todo";
            }
            else
            {
                btsave.Content = "Add Todo";
            }
        }

        private void btsave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               
                string task = tbTask.Text;
                if (!Regex.IsMatch(task, @"^[a-zA-Z\d %_\(\),\.\/\\-]{1,100}$"))
                {
                    MessageBox.Show(this, "Task must be 1-100 characters, made up of letters, digits, space, %_-(),./\\.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                 DateTime dueDate = dpDueDate.SelectedDate.Value;
               // DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                if (dueDate == null)
                {
                    MessageBox.Show(this, "Please select due date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                DialogResult = true; //assign result and dissmiss dialog

            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException || ex is OverflowException || ex is InvalidDataException)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }
    }
}
