﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10TodoEfWithDialog
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public  class Todo
    {

       // [Table ("Todos")]
        public int Id { get; set; }


        [Required] // means non-null
        [StringLength(100)] // nvarchar(50)

        public string Task { get; set; }

        [Required]

        public DateTime DueDate { get; set; }

        [NotMapped]
        public string DueDateCurrCulture => $"{DueDate:d}";// make sure this property never is reflected in the database

        [Required]
        [EnumDataType(typeof(StatusEnum))]

        public StatusEnum Status { get; set; }
        public enum StatusEnum { Pending = 1, Done = 2, Delegated = 3 };

        public override string ToString()
        {
            return $"{Id} {Task} {DueDate:d} {Status}";//{DueDate:d}
        }
       
      
    }

}
