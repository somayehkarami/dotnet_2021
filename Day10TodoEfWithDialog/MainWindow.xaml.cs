﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10TodoEfWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 
        public TodoDBContext ctx;        //open database connection
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                // lvTodoList.ItemsSource = ToList;
                ctx = new TodoDBContext();
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        private void AddTodo_Click(object sender, RoutedEventArgs e)
        {
            TodoDialog dlg = new TodoDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) // confirmed
            {
                try
                {
                    string task = dlg.tbTask.Text;
                    DateTime dueDate = dlg.dpDueDate.SelectedDate.Value;
                    if (dlg.dpDueDate.SelectedDate == null)
                    {
                        MessageBox.Show(this, "Please select due date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                  
                     Todo.StatusEnum status = (Todo.StatusEnum)dlg.comboStatus.SelectedItem;
                    Todo todo = new Todo { Task = task, DueDate = dueDate, Status = status }; // ex ArgumentNullException , ArgumentException, 
                    ctx.Todos.Add(todo);
                    ctx.SaveChanges();
                    lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
                    lblCursorPosition.Text = "New Todo added";
                }
                catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
                {
                    Console.WriteLine(ex.StackTrace);
                    MessageBox.Show("Database operation failed:\n" + ex.Message);
                }

            }
        }
        


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "Do you want to leave?";
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                // If user doesn't want to close, cancel closure.
                e.Cancel = true;
            }
            
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodoList.SelectedValue == null) // (lvToDoList.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please select an item to delete", "Selection error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Todo selTodo = (Todo)lvTodoList.SelectedItem;
            //selectedIndex = lvTodoList.SelectedIndex;

            // See if the user really wants to delete a Todo
            string msg = "Do you want to delete the todo?\n" + selTodo.ToString();
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
               
                
               
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
                
                ctx.Todos.Remove(selTodo);
                ctx.SaveChanges();
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
            }

            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void UpdateTodoClick(object sender, MouseButtonEventArgs e)
        {
            //if (lvTodoList.SelectedItems.Count == 0) return;
            Todo todo = (Todo)lvTodoList.SelectedItem;
            if (todo == null)
            {
                return;
            }
            TodoDialog dlg = new TodoDialog(todo);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) // confirmed
            {
                todo.Task = dlg.tbTask.Text;
                todo.DueDate = (DateTime)dlg.dpDueDate.SelectedDate;
                todo.Status = (Todo.StatusEnum)dlg.comboStatus.SelectedItem;
                // ctx.Todos.Add(todo);
                ctx.SaveChanges();
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
            }
        }
    }
}

