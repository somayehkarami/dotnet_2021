﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrFirstWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string input = tbInput.Text;
            if (___InputCel.Checked == true) && (___OutputFah.Checked == true)
            {


                try
                {
                    double cel = Double.parseDouble(input);
                    double fah = cel * 9 / 5 + 32;
                    String Result = String.format("%.2f Fahrenheit", fah);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            else if (rbCelciusInput.isSelected() && rbKelvinOutput.isSelected())
            {
                try
                {
                    double cel = Double.parseDouble(input);
                    double Kel = cel + 273.15;
                    String Result = String.format("%.2f Kelvin", Kel);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
            else if (rbFahrenheitInput.isSelected() && rbCelciusOutput.isSelected())
            {
                try
                {
                    double fah = Double.parseDouble(input);
                    double cel = (fah - 32) * 5 / 9;
                    String Result = String.format("%.2f Fahrenhei", cel);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
            else if (rbFahrenheitInput.isSelected() && rbKelvinOutput.isSelected())
            {
                try
                {
                    double fah = Double.parseDouble(input);
                    double kel = 273.5f + ((fah - 32.0f) * (5.0f / 9.0f));
                    String Result = String.format("%.2f Fahrenhei", kel);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
            else if (rbKelvinInput.isSelected() && rbCelciusOutput.isSelected())
            {
                try
                {
                    double kel = Double.parseDouble(input);
                    double cel = kel - 273.15;
                    String Result = String.format("%.2f Celcius ", cel);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
            else if (rbKelvinInput.isSelected() && rbFahrenheitOutput.isSelected())
            {
                try
                {
                    double kel = Double.parseDouble(input);
                    double fah = (((kel - 273) * 9 / 5) + 32);
                    String Result = String.format("%.2f Celcius ", fah);
                    tfOutput.setText(Result);
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(this,
                            "Value must be numerical",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }
}
