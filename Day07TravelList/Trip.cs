﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day07TravelList
{

    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
    public class Trip
    {


        //private DateTime departureDate;

        public Trip(string destanation, string name, string passport, DateTime departureDate, DateTime returnDate)
        {
            Destanation = destanation;
            Name = name;
            Passport = passport;
            DepartureDate = departureDate;
            ReturnDate = returnDate;
        }



        private string _destanation;// storage
        public string Destanation// property
        { 

            get { return _destanation; }
            set { _destanation = value; }



        }
        private string _name; // storage
        public string Name// property
        { 
            get
            {
                return _name;
            }
            set
            {

                _name = value;
            }
        }
        private string _passport; // storage
        public string Passport// property
        {
            get
            {
                return _passport;
            }
            set
            {

                _passport = value;
            }
        }

        private DateTime _returnDate; // storage
        public DateTime ReturnDate// property
        {
            get
            {
                return _returnDate;
            }
            set
            {

                _returnDate = value;
            }
        }

        private DateTime _departureDate; // storage
        public DateTime DepartureDate// property
        {
            get
            {
                return _departureDate;
            }
            set
            {

                _departureDate = value;
            }
        }
        public override string ToString()
        {
            return $"{Name} ({Passport}) from {Destanation} to {DepartureDate :d} to {ReturnDate:d}";
        }
    }
}
