﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TravelList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DataFileName = @"..\..\dataTrip.txt";
        List<Trip> TripList = new List<Trip>();
        public MainWindow()
        {
            InitializeComponent();
            lvTripList.ItemsSource = TripList;
            LoadDataFromFile();
           
        }

        private void AddTripButton_Click(object sender, RoutedEventArgs e)
        {
            string destination = tbDestination.Text;
            if (destination.Length < 2 || destination.Length > 30)
            {
                MessageBox.Show(this, "destination must be 2 - 30 char", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string name = tbName.Text;
            if (name.Length < 2 || name.Length > 30)
            {
                MessageBox.Show(this, "Name must not be empty", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string passport = tbPassport.Text;
            if (!Regex.IsMatch(passport, @"^[A-Z]{2}[0-9]{6}$"))
            {
                MessageBox.Show(this, "Passport  must be follow this pattern AA123456", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dbDeparture.SelectedDate == null || dbReturn.SelectedDate == null)
            {
                MessageBox.Show(this, "Departure and Return must not be empty", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DateTime depart = dbDeparture.SelectedDate.Value.Date;
            DateTime returnDate = dbReturn.SelectedDate.Value.Date;
            try
            {
                Trip trip = new Trip(destination, name, passport, depart, returnDate);
                TripList.Add(trip);
                lvTripList.Items.Refresh();
                //TripList.Add(new Trip(name, destination, passport, depart, returnDate));// ex ArgumentException
                //Console.WriteLine(destination, passport, depart, name, returnDate);

                //======================================
                //TripList.Items.Add(trip);
                //Console.WriteLine(destination, passport, depart, name, returnDate);
                // string line = $"{name}{passport}to{destination}on{depart}{returnDate}";
                // lvTripList.Items.Add(line);
                // Clear the inputs
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Error writing file:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SelectedItemButton_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvTripList.SelectedItems.Cast<Trip>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Trips files (*.trips)|*.trips|All files (*.*)|*.*";// File Filter
            saveFileDialog.FilterIndex = 1;                                                     
            if (saveFileDialog.ShowDialog() == true)
            {
                string filename = saveFileDialog.FileName;
                SaveDataToFile(filename, selItems);


            }
            //  File.WriteAllText(saveFileDialog.FileName, selItems.ToString());// FIXME : 

        }
        private void SaveDataToFile((string filename, List<Trip> tripsToSave)
        {
            //string depDate = DateTime.UtcNow.ToString("yyyy-MM-dd")==> Ut ( London)


            try
            {
                List<string> linesList = new List<string>();
                foreach (Trip trip in TripList)
                {
                    linesList.Add($"{trip.Name};{trip.Destanation};{trip.Passport};{trip.ReturnDate:yyyy-MM-dd};{trip.DepartureDate:yyyy-MM-dd}");
                }
                File.WriteAllLines(filename, linesList); // ex IOException, SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                //Console.WriteLine("Error writing file: " + ex.Message);
                MessageBox.Show(this, ex.Message, "Error writing file:", MessageBoxButton.OK, MessageBoxImage.Error);


            }
        }
    

        private void LoadDataFromFile() {
            try
            {
                if (!File.Exists(DataFileName))
                {
                    return; // it's okay if the file does not exist yet
                }
                string[] linesArray = File.ReadAllLines(DataFileName); // ex IOException, SystemException
                foreach (string line in linesArray)
                {
                    try
                    {
                        string[] data = line.Split(';');
                        if (data.Length != 5)
                        {
                            throw new FormatException("Invalid number of items");
                            // or: Console.WriteLine("Error..."); continue;
                        }
                        string destination = data[0];
                        string name = data[1];
                        string passport = data[2];
                        DateTime depart = DateTime.ParseExact(data[3] ,"yyyy-MM-dd", CultureInfo.InvariantCulture);
                        DateTime returnDate = DateTime.ParseExact(data[4], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        Trip trip = new Trip(destination, name, passport, depart, returnDate); // ex ArgumentException
                        TripList.Add(trip);
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        //Console.WriteLine($"Error (skipping line): {ex.Message} in:\n  {line}");
                        MessageBox.Show(this,  ex.Message, "Error (skipping line)", MessageBoxButton.OK, MessageBoxImage.Error);
                        
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
                MessageBox.Show(this, "Error reading file", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(DataFileName, TripList);
        }
    }
}
