﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarOwners
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new DataLayer.CarsOwnersDbContext();// ex SystemException
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        public void ReloadRecords()
        {
            try
            {
                lvOwners.ItemsSource = Globals.ctx.Owners.ToList(); // ex SystemException
                Utils.AutoResizeColumns(lvOwners);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        public void ClearInputs()
        {
            tbName.Text = "";
            imageViewer.Source = null;
            btnDelete.IsEnabled = false;
            btnManageCars.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) return;
            try
            {
                DataLayer.Owner owner = new DataLayer.Owner
                {
                    Name = tbName.Text,
                    Photo = currOwnerImage,
                    //CarsInGarage = new List<DataLayer.Car>()
                };
                Globals.ctx.Owners.Add(owner);//ex
                Globals.ctx.SaveChanges();//ex
                ClearInputs();
                ReloadRecords();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            DataLayer.Owner ownerCurr = (DataLayer.Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            try
            {
                ownerCurr.Name = tbName.Text;
                ownerCurr.Photo = currOwnerImage;
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Owner ownerCurr = (DataLayer.Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + ownerCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // action cancelled
            try
            {
                Globals.ctx.Owners.Remove(ownerCurr);
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnManageCars_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Owner owner = (DataLayer.Owner)lvOwners.SelectedItem;
            if (owner == null) return;
            Carsdialog carDialog = new Carsdialog(owner) { Owner = this };
            carDialog.ShowDialog();
            ReloadRecords();
        }
        bool AreInputsValid()
        {
            List<string> errorsList = new List<string>();
            if (tbName.Text.Length < 2 || tbName.Text.Length > 100)
            {
                errorsList.Add("Name must be between 2 and 100 characters");
            }
            if (currOwnerImage == null)
            {
                errorsList.Add("You must choose a picture");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        byte[] currOwnerImage;
        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            // dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwners.SelectedIndex == -1)
            {
                ClearInputs();
                return;
            }
            try
            {
                DataLayer.Owner owner = (DataLayer.Owner)lvOwners.SelectedItem;
                lblOwnerId.Content = owner.Id;
                tbName.Text = owner.Name;
                currOwnerImage = owner.Photo;
                imageViewer.Source = Utils.ByteArrayToBitmapImage(owner.Photo); // ex
                btnDelete.IsEnabled = true;
                btnUpdate.IsEnabled = true;
                btnManageCars.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
