﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day00Numbers
{
	class Program
	{
		static void Main(string[] args)
		{
			Random random = new Random();
			Console.Write ("how many numbers you wants to generate? ");
			int num = Convert.ToInt32(Console.ReadLine());
			int randomNumber;
			for (int i = 0; i < num; i++)
			{
			
				randomNumber = random.Next(-100, 100);
				Console.WriteLine(randomNumber);
			}
			
			Console.ReadLine();
		}
		
	}
}
