﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TodoAgain
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public class Todo
    {

        public Todo(string dataLine) // ex InvalidDataException
        {
            try
            {
                string[] data = dataLine.Split(';');
                if (data.Length != 4)
                {
                    throw new InvalidDataException("Invalid number of elements in line");
                }
                Task = data[0];

                Difficulty = Convert.ToInt32(data[1]);
                DueDate = DateTime.ParseExact(data[2], "yyyy-MM-dd", CultureInfo.InvariantCulture); // ex
                //============================================

                if (!Enum.TryParse(data[3], out EStatus st))
                {
                    throw new InvalidDataException("Error in Status parsing*****************");
                }
                Status = st;
            }
            //====================================================
            catch (FormatException ex)
            {
                throw new InvalidDataException("Data format invalid in line", ex);
            }
        }

        public Todo(string task, int difficulty, DateTime dueDate, EStatus status)
        {
            Task = task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, %_-(),./\ only
            Difficulty = difficulty; // 1-5, as slider
            DueDate = dueDate; // year 1900-2100 both inclusive, use formatted field
            Status = status; // TheStatus = theStatus;
        }
        private string _task;// storage
        public string Task
        { // property

            get { return _task; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9%_(\),\.\/\\]{1,100}$"))//[a-zA-Z0-9%_\-\,\./\\]
                {
                    throw new InvalidDataException("Task must be 1-100 characters long");
                }
                _task = value;
            }
        }
        private int _difficulty;// storage
        public int Difficulty
        { // property

            get { return _difficulty; }
            set
            {

                if (value < 1 || value > 5)
                {
                    throw new InvalidDataException("Difficulty must be 1-5 ");
                }
                _difficulty = value;
            }
        }
        /* public string DueDateCurrCuiture
         {
             get
             {
                 return $"{_dueDate}";
             }
         }*/
        private DateTime _dueDate;// storage
        public DateTime DueDate
        { // property

            get { return _dueDate; }
            set
            {
                if (value.Year < 1900 || value.Year > 2100)
                {
                    throw new InvalidDataException("year  must be between 1900-2100");
                }
                _dueDate = value;
            }
        }
        private EStatus _status;
        public EStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            return $"{Task} {Difficulty} {DueDate:d} {Status}";
        }
        public string ToDataString()
        {
            return $"{Task};{Difficulty};{DueDate:yyyy-MM-dd};{Status}";// no space
        }


        public enum EStatus
        {
            Pending, Done, Delegated
        }
    }
}
