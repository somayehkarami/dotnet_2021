﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static TodoAgain.Todo;

namespace TodoAgain
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Todo currentTodo;
        //  List<Todo> TodoList = new List<Todo>();
        //declare Mainwindow as a parameter in your child window
        public Todo todo;
        public AddEditDialog(Todo todo = null)
        {
            currentTodo = todo;

            InitializeComponent();
            if (currentTodo != null)
            {

                // change Save button text to "Update"
                btSave.Content = "Update";
                updateTodo();
            }
            else
            {
                // change Save button text to "Create"
                btSave.Content = "Create";
            }
        



    }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Get/Set the inputs:
                string task = tbTask.Text;
                int difficulty = (int)SliderDiff.Value;
                DateTime dueDate = dpDueDate.SelectedDate.Value;

                if (dpDueDate.SelectedDate == null)
                {
                    MessageBox.Show(this, "Please select dueDate ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                EStatus status;

                //   result = Enum.TryParse((string)comboStatus.SelectedValue, out status);
                if (!Enum.TryParse(comboStatus.Text, out status))
                {
                    MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                //
                Todo todo = new Todo(task, difficulty, dueDate, status);//, stat); // ex InvalidDataException
                ((MainWindow)Application.Current.MainWindow).TodoList.Add(todo);
                ((MainWindow)Application.Current.MainWindow).lvTodoList.Items.Refresh();
                // lvTodoList.Items.Refresh(); // inform ListView that data has changed
                // lvTripList.Items.Add(trip);
                // Clear the inputs
                tbTask.Clear();
                lblSlider.Content = "";
                dpDueDate.SelectedDate = DateTime.Today;
                this.Close();

            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //DialogResult = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //SaveDataToFile(DataFileName, TodoList);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                int difficulty = (int)SliderDiff.Value;
                DateTime dueDate = dpDueDate.SelectedDate.Value;
                EStatus status;
                if (!Enum.TryParse(comboStatus.Text, out status))
                {
                    MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                //Todo.EStatus status = (Todo.EStatus)comboStatus.SelectedValue;
                Todo todo = new Todo(task, difficulty, dueDate, status); // ex InvalidDataExceptio
                ((MainWindow)Application.Current.MainWindow).TodoList[((MainWindow)Application.Current.MainWindow).index] = todo;
                ((MainWindow)Application.Current.MainWindow).lvTodoList.Items.Refresh();
                tbTask.Clear();
                lblSlider.Content = "";
                comboStatus.Items.Clear();
                dpDueDate.SelectedDate = DateTime.Today;
                this.Close();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void updateTodo()
        {
            try
            {
                string task = tbTask.Text;
                int difficulty = (int)SliderDiff.Value;
                DateTime dueDate = dpDueDate.SelectedDate.Value;
                EStatus status;
                if (!Enum.TryParse(comboStatus.Text, out status))
                {
                    MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                //Todo.EStatus status = (Todo.EStatus)comboStatus.SelectedValue;
                Todo todo = new Todo(task, difficulty, dueDate, status); // ex InvalidDataExceptio
                ((MainWindow)Application.Current.MainWindow).TodoList[((MainWindow)Application.Current.MainWindow).index] = todo;
                ((MainWindow)Application.Current.MainWindow).lvTodoList.Items.Refresh();
                tbTask.Clear();
                lblSlider.Content = "";
                comboStatus.Items.Clear();
                dpDueDate.SelectedDate = DateTime.Today;
                this.Close();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}

