﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TodoAgain
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DataFileName = @"..\..\data.txt";
        public List<Todo> TodoList = new List<Todo>();
        public int index;
        public MainWindow()
        {
            InitializeComponent();
            lvTodoList.ItemsSource = TodoList; // to connect them
            LoadDataFromFile();


        }
        
        //TODO : add cancel button

        private void miEditAdd_Click(object sender, RoutedEventArgs e)//To Ini other window
        {
            AddEditDialog dlg = new AddEditDialog(null);// class
            dlg.Owner = this;// know the parent
            //  dlg.ShowDialog();//model
            if (dlg.ShowDialog() == true)
            {
                /*string task = dlg.tbTask.Text;
                int difficulty = (int)dlg.SliderDiff.Value;
                DateTime dueDate = dlg.dpDueDate.SelectedDate.Value;
                Todo.EStatus status = (Todo.EStatus)dlg.comboStatus.SelectedValue;
                Todo todo = new Todo(task, difficulty, dueDate, status);//, stat); // ex InvalidDataException
                TodoList.Add(todo);*/
                lvTodoList.Items.Refresh();


            }
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvTodoList.SelectedItems.Cast<Todo>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            // saveFileDialog.Filter = "Text file (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
            saveFileDialog.DefaultExt = ".txt";
            saveFileDialog.FilterIndex = 1;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }


        }
     private  void SaveDataToFile(string fileName, List<Todo> todoToSave)
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (Todo t in todoToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName)) return; // do nothing if the file does not exist yet
                string[] linesList = File.ReadAllLines(DataFileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        Todo todo = new Todo(line); // ex InvalidDataException
                        TodoList.Add(todo);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                File.WriteAllLines(@"..\..\data.txt", linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }



        private void lvTodoList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            try
            {
                AddEditDialog dlg = new AddEditDialog(null);
                dlg.Owner = this;
                Todo td = (Todo)lvTodoList.SelectedItem;
                index = this.lvTodoList.SelectedIndex;
                dlg.tbTask.Text = td.Task;
                dlg.SliderDiff.Value = td.Difficulty;
                dlg.dpDueDate.SelectedDate = td.DueDate;
                dlg.comboStatus.Text = td.Status.ToString();

                if (dlg.ShowDialog() == true)
                {
                    lvTodoList.Items.Refresh();
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }




        private void lvTodoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            int index = this.lvTodoList.SelectedIndex;
            MessageBoxResult result = MessageBox.Show("Are you sure delete this item?"
                , "Delete Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result == MessageBoxResult.Yes)
            {
                TodoList.RemoveAt(index);
                //use the binding to delete the item =>
                //todolist.Remove(lvToDoList.SelectedItem as Todo)
                lvTodoList.Items.Refresh();
                return;
            }
        }

        private void miEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEditDialog dlg = new AddEditDialog(null);
                dlg.Owner = this;
                Todo td = (Todo)lvTodoList.SelectedItem;
                index = this.lvTodoList.SelectedIndex;
                dlg.tbTask.Text = td.Task;
                dlg.SliderDiff.Value = td.Difficulty;
                dlg.dpDueDate.SelectedDate = td.DueDate;
                dlg.comboStatus.Text = td.Status.ToString();

                if (dlg.ShowDialog() == true)
                {



                    /* string task = dlg.tbTask.Text;
                     int difficulty = (int)dlg.SliderDiff.Value;
                     DateTime dueDate = dlg.dpDueDate.SelectedDate.Value;
                     Todo.EStatus status = (Todo.EStatus)dlg.comboStatus.SelectedValue;
                     Todo todo = new Todo(task, difficulty, dueDate, status); // ex InvalidDataExceptio

                     Console.WriteLine(index);
                     TodoList[index] = todo;*/

                    lvTodoList.Items.Refresh();


                    // TodoList.Add(todo); 
                    // lvTodoList.Items.Refresh();
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }



        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(DataFileName, TodoList);
        }
       
    }
}
