﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10Flight
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // public int count;
        const string DataFileName = @"..\..\data.txt";
        public List<Flight> FlightList = new List<Flight>();
        public int index;
        public MainWindow()
        {
            InitializeComponent();
            lvFlightList.ItemsSource = FlightList;
            LoadDataFromFile();
            lblCursorPosition.Text ="Total Flight:  "+ lvFlightList.Items.Count.ToString();
        }

        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            FlightDialog dlg = new FlightDialog();//class
            dlg.Owner = this;//know the parent
            if (dlg.ShowDialog() == true)
            {
                FlightList.Add(dlg.currentFlight);
                lvFlightList.Items.Refresh();
            }
        }
        void SaveDataToFile(string fileName, List<Flight> todoToSave)
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (Flight t in todoToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName)) return; // do nothing if the file does not exist yet
                string[] linesList = File.ReadAllLines(DataFileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        Flight flight = new Flight(line); // ex InvalidDataException
                        FlightList.Add(flight);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                File.WriteAllLines(@"..\..\data.txt", linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "Do you want to leave?";
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
               
                e.Cancel = true;
            }
            else
            {
                SaveDataToFile(DataFileName, FlightList);
            }
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvFlightList.SelectedItems.Cast<Flight>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|All files (*.*)|*.*";
            //saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
            saveFileDialog.FilterIndex = 1;// (same?) saveFileDialog.DefaultExt = ".txt";
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
           
               
        }

        private void miDelete_Click_1(object sender, RoutedEventArgs e)
        {
            int index = this.lvFlightList.SelectedIndex;
            MessageBoxResult result = MessageBox.Show("Are you sure delete this item?"
                , "Delete Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result == MessageBoxResult.Yes)
            {
                FlightList.RemoveAt(index);
                lvFlightList.Items.Refresh();
                return;
            }
        }

        private void miEdit_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Flight fl = (Flight)lvFlightList.SelectedItem;
                index = this.lvFlightList.SelectedIndex;
                FlightDialog dlg = new FlightDialog(fl);
                dlg.Owner = this;
                dlg.tbformCode.Text = fl.FromCode;
                dlg.tbToCode.Text = fl.ToCode;
                dlg.SliderPass.Value = fl.Pass;
                dlg.dpOnDate.SelectedDate = fl.OnDate;
                dlg.comboType.Text = fl.Type.ToString();
                if (dlg.ShowDialog() == true)
                {
                    lvFlightList.Items.Refresh();
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void lvFlightList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Flight fly = (Flight)lvFlightList.SelectedItem;
                index = this.lvFlightList.SelectedIndex;
                FlightDialog dlg = new FlightDialog(fly);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true)
                {

                    lvFlightList.Items.Refresh();
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

