﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Day10Flight.Flight;

namespace Day10Flight
{
    /// <summary>
    /// Interaction logic for FlightDialog.xaml
    /// </summary>
    public partial class FlightDialog : Window
    {
        public Flight currentFlight;
        //public Flight flight;

        public FlightDialog(Flight flight = null)
        {

            InitializeComponent();
            if (flight != null)
            {
                currentFlight = flight;
                tbformCode.Text = flight.FromCode;
                tbToCode.Text = flight.ToCode;
                SliderPass.Value = flight.Pass;
                dpOnDate.SelectedDate = flight.OnDate;
                comboType.Text = flight.Type.ToString();
                btSave.Content = "Update";
                // update();
            }
            else
            {
                btSave.Content = "Create";
            }

        }

        private void AddFlightButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string fromCode = tbformCode.Text;
                string toCode = tbToCode.Text;
                int pass = (int)SliderPass.Value;
                DateTime onDate = dpOnDate.SelectedDate.Value;
                if (dpOnDate.SelectedDate == null)
                {
                    MessageBox.Show(this, "Please select OnDate ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                EType type;
                if (!Enum.TryParse(comboType.Text, out type))
                {
                    MessageBox.Show(this, "Error in type parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (currentFlight == null)
                {
                    currentFlight = new Flight(fromCode, toCode, pass, onDate, type);
                    
                }
                else
                {

                    currentFlight.FromCode = fromCode;
                    currentFlight.ToCode = toCode;
                    currentFlight.Pass = pass;
                    currentFlight.OnDate = onDate;
                    currentFlight.Type = type;

                }
                DialogResult = true;

               

            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void btSaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string fromCode = tbformCode.Text;
                string toCode = tbToCode.Text;
                int pass = (int)SliderPass.Value;
                DateTime onDate = dpOnDate.SelectedDate.Value;
                if (dpOnDate.SelectedDate == null)
                {
                    MessageBox.Show(this, "Please select OnDate ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                EType type;
                if (!Enum.TryParse(comboType.Text, out type))
                {
                    MessageBox.Show(this, "Error in type parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }


                Flight flight = new Flight(fromCode, toCode, pass, onDate, type); // ex InvalidDataException
                ((MainWindow)Application.Current.MainWindow).FlightList[((MainWindow)Application.Current.MainWindow).index] = flight;
                ((MainWindow)Application.Current.MainWindow).lvFlightList.Items.Refresh();

                tbformCode.Clear();
                tbToCode.Clear();
                lblSlider.Content = "";
                dpOnDate.SelectedDate = DateTime.Today;
                comboType.Items.Clear();
                this.Close();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
       
    }
}


