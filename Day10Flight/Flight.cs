﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day10Flight
{

    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public class Flight
    {
        public Flight(string dataLine) // ex InvalidDataException
        {
            try
            {
                string[] data = dataLine.Split(';');
                if (data.Length != 5)
                {
                    throw new InvalidDataException("Invalid number of elements in line");
                }
                FromCode = data[0];
                ToCode = data[1];
                Pass = Convert.ToInt32(data[2]);
                OnDate = DateTime.ParseExact(data[3], "yyyy-MM-dd", CultureInfo.InvariantCulture); // ex
                if (!Enum.TryParse(data[4], out EType ty))
                {
                    throw new InvalidDataException("Error in Pass parsing*****************");
                }
                Type = ty;
            }
            catch (FormatException ex)
            {
                throw new InvalidDataException("Data format invalid in line", ex);
            }
        }
        public Flight(string fromCode, string toCode, int pass, DateTime onDate, EType type)
        {



            FromCode = fromCode;
            ToCode = toCode;
            Pass = pass;
            OnDate = onDate;
            Type = type;
        }

        private string _fromCode;// storage
        public string FromCode
        { // property

            get { return _fromCode; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$"))
                {
                    throw new InvalidDataException("Task must be 1-100 characters long");
                }
                _fromCode = value;
            }
        }
        private string _toCode;// storage
        public string ToCode
        { // property

            get { return _toCode; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$"))
                {
                    throw new InvalidDataException("tocode must be 1-3 characters long upperCase");
                }
                _toCode = value;
            }
        }
        private int _pass;// storage
        public int Pass
        { // property

            get { return _pass; }
            set
            {
                if (value < 1 || value > 5)
                {
                    throw new InvalidDataException("passanger must be 1-5 ");
                }
                _pass = value;
            }
        }
        private DateTime _onDate;// storage
        public DateTime OnDate// property
        { 

            get { return _onDate; }
            set
            {
              
                _onDate = value;
            }
        }
        private EType _type;
        public EType Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public override string ToString()
        {
            return $"{FromCode}{ToCode}{Pass}{OnDate:yyyy-MM-dd}{Type}"; 
        }

        public string ToDataString()
        {
            return $"{FromCode};{ToCode};{Pass};{OnDate:yyyy-MM-dd};{Type}";
        }

        public enum EType { Domestic, International, Private }

    }

}
