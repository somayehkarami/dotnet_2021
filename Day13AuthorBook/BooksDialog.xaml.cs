﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day13AuthorBook
{
    /// <summary>
    /// Interaction logic for BooksDialog.xaml
    /// </summary>
    public partial class BooksDialog : Window
    {
        DataLayer.Author currAthour;
        public BooksDialog(DataLayer.Author author)
        {
            InitializeComponent();
            currAthour = author;
            lblAuthorName.Content = currAthour.Name;
            ReloadCarRecordsForCurrOwner();
        }
        void ReloadCarRecordsForCurrOwner()
        {
            try
            {
                // we don't necessarily need to run a query here because the data has been loaded already
                List<DataLayer.Book> bookList = currAthour.BooksInLibrary.ToList<DataLayer.Book>();
                lvDialogBooks.ItemsSource = bookList;
                Utils.AutoResizeColumns(lvDialogBooks);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        bool AreInputsValid()
        {
            if (tbDialogBookName.Text.Length < 1 || tbDialogBookName.Text.Length > 100)
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        public void ClearDialogInputs()
        {
            tbDialogBookName.Text = "";
            lblDialogId.Content = "";
            btnDialogDelete.IsEnabled = false;
            btnDialogUpdate.IsEnabled = false;
        }

        private void btnDialogAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            try
            {
                DataLayer.Book b = new DataLayer.Book
                {
                    Title = tbDialogBookName.Text,
                    AurthorId = currAthour.Id
                };
                Globals.ctx.Books.Add(b);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            DataLayer.Book bookCurr = (DataLayer.Book)lvDialogBooks.SelectedItem;
            if (bookCurr == null) { return; } // should never happen
            try
            {
                bookCurr.Title = tbDialogBookName.Text;
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Book bookCurr = (DataLayer.Book)lvDialogBooks.SelectedItem;
            if (bookCurr == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + bookCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                Globals.ctx.Books.Remove(bookCurr);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btnDialogDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void lvDialogBooks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDialogBooks.SelectedIndex == -1)
            {
                ClearDialogInputs();
                return;
            }
            DataLayer.Book bookCurr = (DataLayer.Book)lvDialogBooks.SelectedItem;
            lblDialogId.Content = bookCurr.Id;
            tbDialogBookName.Text = bookCurr.Title;
            btnDialogDelete.IsEnabled = true;
            btnDialogUpdate.IsEnabled = true;
        }
    }
    }

