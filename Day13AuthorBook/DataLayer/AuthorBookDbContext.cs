﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13AuthorBook.DataLayer
{
   public class AuthorBookDbContext : DbContext
    {
        const string DbName = "Day13AuthorBook2.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public AuthorBookDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
    }
}
