﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13AuthorBook.DataLayer
{
  public class Author
    {
        public Author()
        {
            // this is not strictly required but it simplifies some of our code later on
            // without it CarsInGarabe will be null (not an empty collection) if there are no Cars
            BooksInLibrary = new HashSet<Book>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public int BooksNumber { get => BooksInLibrary.Count(); }
        [Required]
        public byte[] Photo { get; set; }

        public virtual ICollection<Book> BooksInLibrary { get; set; }

        public override string ToString()
        {
            return $"{Id}, {Name}";
        }
    }
}
