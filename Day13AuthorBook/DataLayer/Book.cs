﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13AuthorBook.DataLayer
{
   public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int AurthorId { get; set; }

        public virtual Author Author { get; set; }

        public override string ToString()
        {
            return $"{Id}, {Title}";
        }

    }
}
