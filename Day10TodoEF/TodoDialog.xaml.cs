﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day10TodoEF
{
    /// <summary>
    /// Interaction logic for TodoDialog.xaml
    /// </summary>
    public partial class TodoDialog : Window
    {
        public Todo currTodo; // null if adding, non-null if editing
       TodoDBContext ctx;

        public TodoDialog(Todo td = null)
        {

            InitializeComponent();
            if (td != null)
            {
                currTodo = td; // save todo being edited
                //lblId.Content = td.Id + "";
                tbTask.Text = td.Task;
                dpDueDate.SelectedDate = td.DueDate;
                comboStatus.Text = td.Status.ToString();
                btsave.Content = "Update Todo";
            }
            else
            {
                btsave.Content = "Add Todo";
            }
        }


        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            
                try
                {
                    //int id = (int)lblId.Content;
                    string task = tbTask.Text;
                    DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                    Todo.StatusEnum status = (Todo.StatusEnum)Enum.Parse(typeof(Todo.StatusEnum), comboStatus.Text, true);
                    if (currTodo == null)
                    {
                        //  currTodo = new Todo(Task= task, DueDate=dueDate, status); // ex ArgumentNullException , ArgumentException, OverflowException
                        currTodo = new Todo { Task = task, DueDate = dueDate, Status = status };
                         //ctx.Todos.Add(currTodo);
                       // ctx.SaveChanges();


                    }
                    else
                    { // ex ArgumentNullException , ArgumentException, OverflowException
                        currTodo.Task = task;
                        currTodo.DueDate = dueDate;
                        currTodo.Status = status;

                    }
                    DialogResult = true; // only dismiss the dialog if there were no exceptions
                }
                catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException || ex is OverflowException || ex is InvalidDataException)
                {
                    MessageBox.Show(ex.Message, "Error Message");
                }
            }
        }


    }

