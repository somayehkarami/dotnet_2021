﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day10TodoEF
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public class Todo
    {
        /* public Todo(int id, string task, DateTime dueDate, StatusEnum status)
         {
             Id = id;
             Task = task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, %_-(),./\ only
                          // 1-5, as slider
             DueDate = dueDate; // year 1900-2100 both inclusive, use formatted field
             Status = status; // TheStatus = theStatus;


     }*/
       /* public Todo( string line)
        {
            try
            {
                var str = line.Split(';'); // ex ArgumentOutOfRangeException, ArgumentException
                if (str.Length == 4)
                {
                    Id = int.Parse(str[0]);
                    Task = str[1];
                    
                    try
                    {
                        DueDate = DateTime.ParseExact(str[2], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex) when (ex is ArgumentNullException || ex is FormatException)
                    {
                        throw new InvalidDataException(ex.Message);
                    }
                    if (!Enum.TryParse(str[3], out StatusEnum status))
                    {
                        throw new InvalidDataException("Error in Status parsing*****************");
                    }
                    Status = status;
                }
            
                else
                {
                    throw new InvalidDataException("Data string should be 4 items long.");
                }
            }
            catch (Exception ex) when (ex is ArgumentOutOfRangeException || ex is ArgumentException)
            {
                throw new InvalidDataException("Data string is invalid");
            }
        }*/



        public int Id { get; set; }
        

        [Required] // means non-null
        [StringLength(100)] // nvarchar(50)
      
        public string Task { get; set; }
        
        [Required]
     
        public DateTime DueDate { get; set; }
        
        [Required]
        [EnumDataType(typeof(StatusEnum))]
      
        public StatusEnum Status { get; set; }
        public enum StatusEnum { Pending = 1, Done = 2, Delegated = 3 };

        public override string ToString()
        {
            return $"{Id} {Task} {DueDate:d} {Status}";//{DueDate:d}
        }
        public string ToDataString()
        {
            return $"{Id};{Task};{DueDate:yyyy-MM-dd};{Status}";//{DueDate:yyyy-MM-dd}
        }
    }
}
