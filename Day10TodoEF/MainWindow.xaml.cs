﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10TodoEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal ObservableCollection<Todo> listTodo = new ObservableCollection<Todo>();
        
        const string Path = @"..\..\todos.txt";
       public TodoDBContext ctx;        //open database connection
       
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                // lvTodoList.ItemsSource = ToList;
                ctx = new TodoDBContext();
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
                
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        private void AddTodo_Click(object sender, RoutedEventArgs e)
        {
            TodoDialog dlg = new TodoDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) // confirmed
            {
                ctx.Todos.Add(dlg.currTodo);
                ctx.SaveChanges();
                lvTodoList.ItemsSource = (from t in ctx.Todos select t).ToList<Todo>();
                lblCursorPosition.Text = "New Todo added";
            }
        }
      /*   private void LoadDataFromFile()
        {
            try
            {
                using (StreamReader sr = File.OpenText(Path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        try
                        {
                            Todo td = new Todo(s);
                            listTodo.Add(td);
                        }
                        catch (InvalidDataException ex)
                        {
                            MessageBox.Show(ex.Message, "Error messsage");
                        }
                    }
                }

            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error loading from file", "Error messsage");
            }
        }*/
        private void SaveDataToFile(string path, IList list = null)
        {
            try
            {
                if (list is null)
                {
                    list = listTodo;
                }

                List<string> str = new List<string>();

                // Save document
                foreach (Todo tp in list)
                {
                    str.Add(tp.ToDataString());
                }
                File.WriteAllLines(path, str);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Writing to file error", "Error Message");
            }
        }

        private void SaveSelectedTodos(object sender, RoutedEventArgs e)
        {
            if (lvTodoList.SelectedItems.Count > 0)
            {
                var selItems = lvTodoList.SelectedItems;

                // Configure save file dialog box
                SaveFileDialog dlg = new SaveFileDialog { Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*" }; // Filter files by extension

                //dlg.FileName = "todo"; // Default file name
                dlg.DefaultExt = ".txt"; // Default file extension

                // Process save file dialog box results
                if (dlg.ShowDialog() == true)
                {
                    SaveDataToFile(dlg.FileName, selItems);
                    lblCursorPosition.Text = "Selected Todo is saved";
                    lvTodoList.UnselectAll();
                }
            }
            else
            {
                MessageBox.Show("Nothing is selected", "Warning:");
            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "Do you want to leave?";
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                // If user doesn't want to close, cancel closure.
                e.Cancel = true;
            }
            else
            {
                SaveDataToFile(Path);
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
