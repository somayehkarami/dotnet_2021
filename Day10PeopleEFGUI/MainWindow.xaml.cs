﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10PeopleEFGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PoepleDbContext ctx;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new PoepleDbContext();
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();// to show view List move it here

            }
            catch(SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

            string Name = lblName.Text;
            if (!int.TryParse(lblAge.Text , out int age ))
            {
                MessageBox.Show("Age invaild");
                return;

            }
            try
            {
                Person person = new Person { Name = Name, Age = age };
                ctx.People.Add(person);
                ctx.SaveChanges();
                lblName.Text = "";
                lblAge.Text = "0";
                // refresh the list for database
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation faield:\n" +ex.Message);
            }

        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            string name = lblName.Text;
            if (!int.TryParse(lblAge.Text, out int age))
            {
                MessageBox.Show("Age invaild");
                return;

            }
            try
            {
                //Person person = new Person { Name = Name, Age = age };Remove this part for update
                currSelPerson.Name = name;
                currSelPerson.Age = age;
                ctx.SaveChanges();
                lblName.Text = "";
                lblAge.Text = "";
                // refresh the list for database
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation faield:\n" + ex.Message);
            }

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {

            if (currSelPerson == null) return;
            var result = MessageBox.Show(this, "Are you sure you want to delete this record?\n" +
              currSelPerson, "Confirmation requried", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);


            if (result == MessageBoxResult.OK)
                {
                ctx.People.Remove(currSelPerson); // schedule attached entity for deletion
                ctx.SaveChanges();
                lvPeople.ItemsSource = (from p in ctx.People select p).ToList<Person>();
            }

        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        Person currSelPerson;
        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelPerson = lvPeople.SelectedItem as Person;
            if(currSelPerson == null)
            {
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                lblId.Content = "-";
                lblName.Text = "";
                lblAge.Text = "";

            }
            else
            {
                btUpdate.IsEnabled = true;
                btDelete.IsEnabled = true;
                lblId.Content = currSelPerson.Id;
                lblName.Text = currSelPerson.Name;
                lblAge.Text = currSelPerson.Age + "";
            }
        }
    }
}
