﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03FirstIndexer

{
    class PrimeArray
    {
        public bool this[long index]
        {
            get
            {
                return isPrime(index);
            }
        }
        private bool isPrime(long num)
        {
            for (int div = 2; div <= num / 2; div++)
            {
                if (num % div == 0)
                {
                    return false;
                }
            }
            return true;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PrimeArray pa = new PrimeArray();
                for (int i = 2; i < 50; i++)
                {
                    bool result = pa[i];
                    if (result)
                    {
                        Console.WriteLine($"Number {i} is a prime number");
                    }

                }
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
//BigInterger