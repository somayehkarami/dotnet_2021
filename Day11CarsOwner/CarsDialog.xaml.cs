﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwner
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    ///
    public partial class CarsDialog : Window
    {
        CarDBContext ctx;
        public CarsDialog()
        {
            InitializeComponent();
            ctx = new CarDBContext();
            lvCars.ItemsSource = (from c in ctx.Cars select c).ToList<Car>();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            //selectedOwner.Name = tbName.Text;
           // string MakeModel = tbModel.Text;
            try
            {
               
                currSelCar.MakeModel = tbModel.Text;
                ctx.SaveChanges();
                tbModel.Text = "";
                lvCars.ItemsSource = (from c in ctx.Cars select c).ToList<Car>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation faield:\n" + ex.Message);
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string MakeModel = tbModel.Text;
            try
            {
                Car car = new Car { MakeModel = MakeModel }; //Owner= ob };
                ctx.Cars.Add(car);
                ctx.SaveChanges();
                tbModel.Text = "";
                lvCars.ItemsSource = (from c in ctx.Cars select c).ToList<Car>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation faield:\n" + ex.Message);
            }

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {

            if (currSelCar == null) return;
            var result = MessageBox.Show(this, "Are you sure you want to delete this record?\n" +
             currSelCar, "Confirmation requried", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);


            if (result == MessageBoxResult.OK)
            {
                ctx.Cars.Remove(currSelCar);
                ctx.SaveChanges();
                lvCars.ItemsSource = (from c in ctx.Cars select c).ToList<Car>();
            }
        }

        Car currSelCar;
        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelCar = lvCars.SelectedItem as Car;
            if (currSelCar == null)
            {
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                tbModel.Text = "";

            }
            else
            {
                btUpdate.IsEnabled = true;
                btDelete.IsEnabled = true;
                tbModel.Text = currSelCar.MakeModel;
            }
        }
    }
}