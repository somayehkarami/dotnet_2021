using System;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Day11CarsOwner
{
    public class CarDBContext : DbContext
    {
        const string DbName = "Database2.mdf";

        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        // static string DbPath = Path.Combine(Environment.CurrentDirectory + @"\..\..\", DbName);
        public CarDBContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

         public virtual DbSet<Owner> Owners{ get; set; }
        public virtual DbSet<Car> Cars { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}