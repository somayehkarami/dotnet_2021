﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwner
{
    public class Owner
    {

        public int Id { get; set; }

        [Required] // means non-null
        [StringLength(50)] // nvarchar(50)
        public string Name { get; set; }

        [NotMapped]
        public int CarNumber
        {
            get
            {
                return CarsInGarage.Count();
            }
        }

        public byte[] Photo { get; set; }

      public virtual  ICollection<Car> CarsInGarage { get; set; } = new HashSet<Car>(); // one to many



    }


   public class Car
    {
        public int Id { get; set; }
        [Required] // means non-null
        [StringLength(50)] // nvarchar(50)
        public string MakeModel { get; set; }
        public Owner Owner { get; set; }

    }
}
