﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarDBContext ctx;
        Owner selectedOwner;

        // public byte[] selectedImage;
        public byte[] currentImage;

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new CarDBContext();
                lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();
                //ICollection<Car> CarsInGarage = ctx.Cars.Include(c => c.Owners).ToList();

            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }


        public byte[] imageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }


        public BitmapImage byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = ms;
            bmp.EndInit();
            return bmp;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;

            if (name.Length < 2 || name.Length > 50)
            {
                MessageBox.Show("Name must be between 2 - 50.");
                return;
            }

            Owner ob = new Owner { Name = name, Photo = currentImage };
            ctx.Owners.Add(ob);
            ctx.SaveChanges();
            lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();

        }

        private void btPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp; *.png" };
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    currentImage = File.ReadAllBytes(openFileDialog.FileName);
                    tbImage.Visibility = Visibility.Hidden; // hide the text box
                    BitmapImage bitmap = byteArrayToImage(currentImage); // ex: SystemException
                    imagePreview.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btManage_Click(object sender, RoutedEventArgs e)
        {
            CarsDialog dlg = new CarsDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {

            }
        }

        private void lvOwners_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            selectedOwner = lvOwners.SelectedItem as Owner;
            if (selectedOwner == null)
            {
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
                tbName.Text = "";
            }

            else
            {
                btUpdate.IsEnabled = true;
                btDelete.IsEnabled = true;
                lblId.Content = selectedOwner.Id;
                tbName.Text = selectedOwner.Name;
                currentImage = selectedOwner.Photo;
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (selectedOwner == null) return;
            var result = MessageBox.Show(this, "Are you sure you want to delete this record?\n" +
           selectedOwner, "Confirmation requried", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            ctx.Owners.Remove(selectedOwner);
            ctx.SaveChanges();
            lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedOwner.Name = tbName.Text;
            currentImage = selectedOwner.Photo;
            //selectedOwner.Photo = tbImage.;//what did you write here?
            if (currentImage != null)
            {
                try
                {
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = byteArrayToImage(currentImage); // ex: SystemException
                    imagePreview.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ctx.SaveChanges();
                tbName.Text = "";
                lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();

            }
        }


    }
}


