﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Day08TodoGui.Todo;

namespace Day08TodoGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //DisplayMemberBinding="{Binding DueDate, StringFormat=\{0:d\}}"
        //comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.EStatus)).Cast<Todo.EStatus>();
        //  comboStatus.SelectedIndex = 0;

        const string DataFileName = @"..\..\data.txt";

        List<Todo> TodoList = new List<Todo>();
        public MainWindow()
        {

            InitializeComponent();
            LoadDataFromFile();

            lvTodoList.ItemsSource = TodoList;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Get/Set the inputs:
                string task = tbTask.Text;
                /*difficulty;
                 if (!int.TryParse(SliderDiff.Value, out  difficulty))
                 {
                     MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                 }*/
                int difficulty = (int)SliderDiff.Value;
                //int difficulty = Convert.ToInt32(SliderDiff.Value);
                DateTime dueDate = dpDueDate.SelectedDate.Value;

                if (dpDueDate.SelectedDate == null)
                {
                    MessageBox.Show(this, "Please select dueDate ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                EStatus status;

                //   result = Enum.TryParse((string)comboStatus.SelectedValue, out status);
                if (!Enum.TryParse(comboStatus.Text, out status))
                {
                    MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }



                //
                Todo todo = new Todo(task, difficulty, dueDate, status);//, stat); // ex InvalidDataException
                TodoList.Add(todo);
                lvTodoList.Items.Refresh(); // inform ListView that data has changed
                                            // lvTripList.Items.Add(trip);
                                            // Clear the inputs
                tbTask.Clear();
                lblSlider.Content = "";

                dpDueDate.SelectedDate = null;


            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void SaveDataToFile(string fileName, List<Todo> todoToSave)
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (Todo t in todoToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName)) return; // do nothing if the file does not exist yet
                string[] linesList = File.ReadAllLines(DataFileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        Todo todo = new Todo(line); // ex InvalidDataException
                        TodoList.Add(todo);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                File.WriteAllLines(@"..\..\data.txt", linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(DataFileName, TodoList);
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvTodoList.SelectedItems.Cast<Todo>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Trips files (*.txt)|*.txt|All files (*.*)|*.*";
            //saveFileDialog.Filter = "Text Files | *.txt";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {

           
                var selectedItems = lvTodoList.SelectedItems.Cast<Todo>().ToList();

                foreach (var item in selectedItems)
                {
                    TodoList.Remove(item);
                }
                lvTodoList.Items.Refresh();
            
        }
        private void update()
        {
            try
            {
                // Get/Set the inputs:
                string task = tbTask.Text;
                int difficulty = Convert.ToInt32(SliderDiff.Value);
                DateTime dueDate = dpDueDate.SelectedDate.Value;

                if (dpDueDate.SelectedDate == null)
                {
                    MessageBox.Show(this, "Please select dueDate ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                EStatus status;

                if (!Enum.TryParse((string)comboStatus.SelectedValue, out status))
                {
                    MessageBox.Show(this, "Error in Status parsing", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);

                }

                //
                Todo todo = new Todo(task, difficulty, dueDate, status); // ex InvalidDataException
                //ED: this is the difference between add and update
                //TodoList.Add(todo);
                var index = lvTodoList.SelectedIndex;
                TodoList[index] = todo;
                lvTodoList.Items.Refresh(); // inform ListView that data has changed
                                            // lvTripList.Items.Add(trip);
                                            // Clear the inputs
                tbTask.Clear();
                lblSlider.Content = "";
                dpDueDate.SelectedDate = null;

            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }


}
